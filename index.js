
/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.
*/
function addition(sum1, sum2){
	console.log("Displayed sum of 5 and 15:");
	console.log(sum1+sum2);
}
addition(5,15);

/*
		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function
*/
function subtraction(sum1, sum2){
	console.log("Displayed difference of 20 and 5:");
	console.log(sum1-sum2);
}
subtraction(20,5);


/*

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.
*/
function multiply(product0, product2){
	console.log("The product of 50 and 10:");
	console.log(product0*product2);
	return(multiply);
}
multiply(50,10);


/*

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.
*/
function division(quotient1, quotient2){
	console.log("The quotient of 50 and 10:");
	console.log(quotient1/quotient2);
	return(division);
}
division(50,10);

/*
	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.
*/
function areaOfCircle(raduis1,raduis2,circle ){
	console.log("The quotient of 50 and 10:");
	console.log(raduis1*raduis2*circle);
	return(areaOfCircle);
}
areaOfCircle(15,15,3.1416);


/*
	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
*/
function averageVar( first, second, third,four){
 let averageVar = first + " " + second + " " + third + " " +  four; 
	 	console.log("The average of " + averageVar);
 		let average = (first+second + third +four)/4;
		return average;
}
 	let totalAverage = averageVar(20,40,60,80);
 	console.log(totalAverage);




// console.log("The Average of "+totalAverage);

 	// let totalAverage = averageVar("20", "30", "40","60");
 	// 

 // function average( first, second, third,four){
 // 	 console.log((first+second + third +four)/4)
 // }
 // 	average(20,30,40,60);
 	

 	
/*
	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/
function passGrade(num){
	let passing = num % 50
	console.log("Is 35/50 passing score?");
	let passGrade = passing < 50;
	console.log(passGrade);
}

passGrade(35);